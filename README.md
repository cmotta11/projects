Projects
========

This is the Projects repository for [CDT-30020-SP16].

Group
-----

The members of this group are:

- Leonardo (leonardo@nd.edu)
- Raphael (raphael@nd.edu)
- Donatello (donatello@nd.edu)
- Michelangelo (michelangelo@nd.edu)

[CDT-30020-SP16]: https://www3.nd.edu/~pbui/teaching/cdt.30020.sp16/
